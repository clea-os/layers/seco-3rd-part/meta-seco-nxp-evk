FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

DEPENDS += "u-boot-mkimage-native"

BOOTSCR_NAME:mx8-nxp-bsp = "seco_bootscr_imx8.txt"
BOOTSCR_NAME:mx9-nxp-bsp = "seco_bootscr_imx9.txt"

SRC_URI += " \
    file://0001-SYSDATA-Add-support-with-load-at-runtime.patch \
    file://0002-SYSDATA-Add-sysdata-command-line-tool.patch \
    file://0003-ENVIRONMENT-Change-environment-to-make-it-compatible.patch \
    file://0004-SYSDATA-TOOL-Add-userspace-SYSDATA-tool.patch \
    file://sysdata.cfg \
"
SRC_URI += "file://${BOOTSCR_NAME}"

ARCHIT:mx8-nxp-bsp = "arm64"
ARCHIT:mx9-nxp-bsp = "arm64"

do_deploy:append() {
    mkimage -A ${ARCHIT} -O linux -T script -d ${WORKDIR}/${BOOTSCR_NAME} ${DEPLOYDIR}/seco_boot.scr
}
