SUMMARY = "U-Boot bootloader SYSDATA utilities"

require recipes-bsp/u-boot/u-boot.inc
require recipes-bsp/u-boot/u-boot-imx-common_${PV}.inc

DEPENDS += "mtd-utils flex-native bison-native"

PROVIDES = "fw-sysdata"
RPROVIDES:${PN} = "fw-sysdata"

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
SRC_URI += " \
    file://0001-SYSDATA-Add-support-with-load-at-runtime.patch \
    file://0002-SYSDATA-Add-sysdata-command-line-tool.patch \
	file://0004-SYSDATA-TOOL-Add-userspace-SYSDATA-tool.patch \
    file://sysdata.cfg \
"

INSANE_SKIP:${PN} = "already-stripped"
EXTRA_OEMAKE:class-target = 'CROSS_COMPILE=${TARGET_PREFIX} CC="${CC} ${CFLAGS} ${LDFLAGS}" HOSTCC="${BUILD_CC} ${BUILD_CFLAGS} ${BUILD_LDFLAGS}" V=1'
EXTRA_OEMAKE:class-cross = 'HOSTCC="${CC} ${CFLAGS} ${LDFLAGS}" V=1'

inherit uboot-config

do_compile () {
	oe_runmake -C ${S} O=${B} ${UBOOT_MACHINE}
	oe_runmake -C ${S} O=${B} sysdatatools
}

do_install () {
	install -d ${D}${base_sbindir}
	install -m 755 ${B}/tools/sysdata/fw_sysdata ${D}${base_sbindir}
}

do_install_class-cross () {
	install -d ${D}${bindir_cross}
	install -m 755 ${B}/tools/sysdata/fw_sysdata ${D}${bindir_cross}/fw_sysdata
}

do_deploy[noexec] = "1"

FILES:${PN} = " \
	/usr \
	/usr/sbin \
	/usr/sbin/fw_sysdata \
"

SYSROOT_DIRS:append_class-cross = " ${bindir_cross}"

BBCLASSEXTEND = "cross"

PACKAGE_ARCH = "${MACHINE_ARCH}"
COMPATIBLE_MACHINE:class-target = "(imx-generic-bsp)"
