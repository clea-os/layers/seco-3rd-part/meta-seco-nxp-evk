# We have a conf and classes directory, add to BBPATH
BBPATH .= ":${LAYERDIR}"

RKBASE = '${@os.path.normpath("${LAYERDIR}")}'

# We have recipes-* directories, add to BBFILES
BBFILES += "${LAYERDIR}/recipes-*/*/*.bb \
	    ${LAYERDIR}/recipes-*/*/*.bbappend"

BBFILES_DYNAMIC += " \
    chromium-browser-layer:${LAYERDIR}/dynamic-layers/chromium-browser-layer/*/*/*.bb \
    chromium-browser-layer:${LAYERDIR}/dynamic-layers/chromium-browser-layer/*/*/*.bbappend \
    \
    qt5-layer:${LAYERDIR}/dynamic-layers/qt5-layer/*/*/*.bb \
    qt5-layer:${LAYERDIR}/dynamic-layers/qt5-layer/*/*/*.bbappend \
"

BBFILE_COLLECTIONS += "seco-nxp"
BBFILE_PATTERN_seco-nxp := "^${LAYERDIR}/"
BBFILE_PRIORITY_seco-nxp = "9"

HOSTTOOLS_NONFATAL:append = " bc rsync xxd lz4c pzstd zstd "


# This should only be incremented on significant changes that will
# cause compatibility issues with other layers
LAYERVERSION_seco-nxp = "1"
LAYERSERIES_COMPAT_seco-nxp = "dunfell kirkstone mickledore nanbield"

MACHINE_USES_VIVANTE_KERNEL_DRIVER_MODULE ?= "0"


# Use latest SDMA firmware from firmware-imx instead of upstream linux-firmware
#MACHINE_FIRMWARE_remove_mx6  = "linux-firmware-imx-sdma-imx6q"
#MACHINE_FIRMWARE_remove_mx7d = "linux-firmware-imx-sdma-imx7d"
#MACHINE_FIRMWARE_append_mx6  = " firmware-imx-sdma firmware-imx-regulatory"
#MACHINE_FIRMWARE_append_mx7  = " firmware-imx-sdma firmware-imx-regulatory"

BBMASK += "${LAYERDIR}/recipes-bsp/grub*"


DISTRO_FEATURES:append = " usrmerge "

IMAGE_FSTYPES = "cpio.gz.u-boot"

RUST_VERSION = "1.75.0"
PREFERRED_VERSION_cargo ?= "${RUST_VERSION}"
PREFERRED_VERSION_cargo-native ?= "${RUST_VERSION}"
PREFERRED_VERSION_libstd-rs ?= "${RUST_VERSION}"
PREFERRED_VERSION_rust ?= "${RUST_VERSION}"
PREFERRED_VERSION_rust-cross-${TARGET_ARCH} ?= "${RUST_VERSION}"
PREFERRED_VERSION_rust-llvm ?= "${RUST_VERSION}"
PREFERRED_VERSION_rust-llvm-native ?= "${RUST_VERSION}"
PREFERRED_VERSION_rust-native ?= "${RUST_VERSION}"

#include conf/distro/include/fsl-imx-base.inc
#include conf/distro/include/fsl-imx-preferred-env.inc

PREFERRED_PROVIDER_virtual/bootloader:imx-nxp-bsp = "u-boot-imx"
PREFERRED_PROVIDER_virtual/kernel:imx-nxp-bsp = "linux-imx"
PREFERRED_PROVIDER_linux-imx-mfgtool = "linux-imx-mfgtool"

PREFERRED_VERSION_gstreamer1.0              = "1.22.5.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-base = "1.22.5.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-good = "1.22.5.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-bad  = "1.22.5.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-ugly = "1.22.%"
PREFERRED_VERSION_gstreamer1.0-libav        = "1.22.%"
PREFERRED_VERSION_gstreamer1.0-rtsp-server  = "1.22.%"

PREFERRED_VERSION_gstreamer1.0:mx8-nxp-bsp              = "1.22.5.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-base:mx8-nxp-bsp = "1.22.5.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-good:mx8-nxp-bsp = "1.22.5.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-bad:mx8-nxp-bsp  = "1.22.5.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-ugly:mx8-nxp-bsp = "1.22.%"
PREFERRED_VERSION_gstreamer1.0-libav:mx8-nxp-bsp        = "1.22.%"
PREFERRED_VERSION_gstreamer1.0-rtsp-server:mx8-nxp-bsp  = "1.22.%"

PREFERRED_VERSION_gstreamer1.0:mx9-nxp-bsp              = "1.22.5.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-base:mx9-nxp-bsp = "1.22.5.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-good:mx9-nxp-bsp = "1.22.5.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-bad:mx9-nxp-bsp  = "1.22.5.imx"
PREFERRED_VERSION_gstreamer1.0-plugins-ugly:mx9-nxp-bsp = "1.22.%"
PREFERRED_VERSION_gstreamer1.0-libav:mx9-nxp-bsp        = "1.22.%"
PREFERRED_VERSION_gstreamer1.0-rtsp-server:mx9-nxp-bsp  = "1.22.%"

BBMASK += "meta-freescale/recipes-multimedia/gstreamer"
BBMASK += "meta-arm/meta-arm/recipes-kernel/linux/linux-yocto_5.15%.bbappend"

PREFERRED_VERSION_opengl-es-cts:mx6-nxp-bsp = "3.2.4.0"
PREFERRED_VERSION_opengl-es-cts:mx7-nxp-bsp = "3.2.4.0"
PREFERRED_VERSION_opengl-es-cts:aarch64 = "3.2.9.3"

POKY_DEFAULT_DISTRO_FEATURES:remove = "${POKY_DEFAULT_DISTRO_FEATURES_IMX_REMOVE}"
POKY_DEFAULT_DISTRO_FEATURES_IMX_REMOVE             ?= "wayland"
POKY_DEFAULT_DISTRO_FEATURES_IMX_REMOVE:mx6-nxp-bsp ?= "wayland vulkan"
POKY_DEFAULT_DISTRO_FEATURES_IMX_REMOVE:mx7-nxp-bsp ?= "wayland vulkan"
DISTRO_FEATURES:remove = "pulseaudio"

POKY_DEFAULT_DISTRO_FEATURES:append = " ${POKY_DEFAULT_DISTRO_FEATURES_IMX_APPEND}"
POKY_DEFAULT_DISTRO_FEATURES_IMX_APPEND             ?= "jailhouse"
POKY_DEFAULT_DISTRO_FEATURES_IMX_APPEND:mx6-nxp-bsp ?= ""
POKY_DEFAULT_DISTRO_FEATURES_IMX_APPEND:mx7-nxp-bsp ?= ""

# Set a more generic tuning for code reuse across parts
DEFAULTTUNE:mx8-nxp-bsp    ?= "armv8a-crc-crypto"
DEFAULTTUNE:mx8m-nxp-bsp   ?= "armv8a-crc-crypto"
DEFAULTTUNE:mx8qm-nxp-bsp  ?= "armv8a-crc-crypto"
DEFAULTTUNE:mx8x-nxp-bsp   ?= "armv8a-crc-crypto"
DEFAULTTUNE:mx8ulp-nxp-bsp ?= "armv8a-crc-crypto"
DEFAULTTUNE:mx9-nxp-bsp    ?= "armv8a-crc-crypto"
DEFAULTTUNE:mx91p-nxp-bsp  ?= "armv8a-crc-crypto"
DEFAULTTUNE:mx93-nxp-bsp   ?= "armv8a-crc-crypto"


WKS_FILE:mx6-nxp-bsp = "sdimage-rauc-imx6.wks.in"
WKS_FILE:mx8-nxp-bsp = "sdimage-rauc-imx8.wks.in"
WKS_FILE:mx9-nxp-bsp = "sdimage-rauc-imx9.wks.in"


PREFERRED_PROVIDER_fw-sysdata = "u-boot-imx-fw-sysdata"
PREFERRED_VERSION_fw-sysdata = "2023.04%"

IMAGE_EDGEHOG_BOOT_FILES_EXTRA = " \
    seco_boot.scr \
"
MACHINEOVERRIDES =. "seco-arm:"
