source sources/meta-imx/tools/imx-setup-release.sh $@

if [ -f conf/local.conf ]; then
    {
cat << EOF

####### EDGEHOG SETTINGS #######

#ASTARTE_URL = ""
#ASTARTE_REALM = ""
#ASTARTE_PAIRING_TOKEN = ""
EOF
    } >> conf/local.conf
fi

if [ -f conf/bblayers.conf ]; then
    {
cat << EOF 

####### EDGEHOG SETTINGS #######
BBLAYERS:remove = "\\
    \${BSPDIR}/sources/meta-nxp-demo-experience \\
"

BBLAYERS += "\${BSPDIR}/sources/meta-seco/meta-seco-nxp-evk"
BBLAYERS += "\\
  \${BSPDIR}/sources/meta-rauc \\
  \${BSPDIR}/sources/meta-seco/meta-seco-edgehog-things \\
"
EOF
    } >> conf/bblayers.conf
fi
